// 1 Прототипне наслідування дозволяє успадковувати характеристики батьківського елемента його дітям
// 2 Метод super() потрібно викликати для того щоб можна було додати нащадку якусь додаткову характеристику

class Employee {
  constructor(settings) {
    (this.name = settings.name),
      (this.age = settings.age),
      (this.salary = settings.salary);
  }

  get getName() {
    return this.name;
  }

  set setName(value) {
    this.name = value;
  }

  get getAge() {
    return this.age;
  }

  set setAge(value) {
    this.age = value;
  }

  get getSalary() {
    return this.salary;
  }

  set setSalary(value) {
    this.salary = value;
  }
}

class programmer extends Employee {
  constructor(settings) {
    super(settings);
    this.langs = settings.langs;
  }

  get salaryCounter() {
    return this.salary * 3;
  }

  set salaryCounter(value) {
    this.salary = value;
  }
}

const firstUser = new Employee({
  name: "dima",
  age: 20,
  salary: 15000,
});

const programmer1 = new programmer({
  name: "Andrei",
  age: 19,
  salary: 20000,
  langs: "JS,C#,C++,Python",
});

const programmer2 = new programmer({
  name: "Misha",
  age: 29,
  salary: 40000,
  langs: "JS,C#,C++,Python,Java",
});

const programmer3 = new programmer({
  name: "Anton",
  age: 20,
  salary: 10000,
  langs: "JS,C#",
});

console.log("перше завдання:", firstUser);
console.log("перше завдання:", firstUser.getAge);

console.log("друге завдання:", firstUser.getName);
console.log("друге завдання:", firstUser.getAge);
console.log("друге завдання:", firstUser.getSalary);

console.log("третє завдання:", programmer1);

console.log("четверте завдання:", programmer1.salaryCounter);

console.log(
  "п'яте завдання:",
  programmer2,
  "Гетер:",
  programmer2.salaryCounter
);

console.log(
  "п'яте завдання:",
  programmer3,
  "Гетер:",
  programmer3.salaryCounter
);
