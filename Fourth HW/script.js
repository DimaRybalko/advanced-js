fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => {
    if (!response.ok) {
      throw new Error(response.status);
    }
    return response.json();
  })
  .then((data) => {
    data.forEach((element) => {
      const { episodeId, name, openingCrawl, characters } = element;
      const root = document.createElement("div");
      root.classList.add("card");
      const tittle = document.createElement("p");
      const number = document.createElement("p");
      const description = document.createElement("p");
      const chars = document.createElement("p");
      root.append(tittle);
      root.append(chars);
      root.append(number);
      root.append(description);
      tittle.append(`Назва епізоду:${name}`);
      number.append(`Айді епізоду:${episodeId}`);
      description.append(`Опис епізоду:${openingCrawl}`);
      document.body.append(root);

      characters.forEach((character) => {
        fetch(character)
          .then((response) => {
            if (!response.ok) {
              throw new Error(response.status);
            }
            return response.json();
          })
          .then((charData) => {
            chars.append(`${charData.name},`);
          });
      });
    });
  });
