// const promise = new Promise((resolve, reject) => {
//   // Виконання асинхронної операції
//   setTimeout(() => {
//     const randomNumber = Math.random();
//     if (randomNumber > 0.5) {
//       // Виконано успішно
//       resolve(randomNumber);
//     } else {
//       // Відхилено з помилкою
//       reject(new Error("Генерація числа не вдалася"));
//     }
//   }, 1000);
// });
// // Обробка результату
// promise
//   .then((result) => {
//     const wrapper = document.createElement("div");
//     wrapper.innerHTML = result;
//     document.body.append(wrapper);
//     console.log(result);
//     console.log("Успішно:", result * 2);
//   })
//   .catch((error) => {
//     console.error("Помилка:", error);
//   })
//   .finally(() => {
//     console.log("I am finally");
// });

// console.log("Початок");

// const promise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     console.log("Асинхронний код завершився");
//     resolve(); // Обіцяємо виконання
//   }, 2000);
// });

// promise.then(() => {
//   console.log("Проміс виконався");
// });
// console.log("Кінець");

// Створіть функцію, яка повертає проміс, який виконується
// через певну кількість мілісекунд і повертає текст "Виконано" після виконання.

// function prom(ms) {
//   return new Promise((resolve) => {
//     setTimeout(() => {
//       resolve("Успішно");
//     }, ms);
//   });
// }

// prom(1000).then((result) => {
//   console.log(result);
// });

// Створіть функцію, яка відкладає виконання проміса
// на випадковий час від 1 до 5 секунд і повертає результат "Готово" після виконання.

// const randomMs = 3000;
// function delay() {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       if (randomMs > 2000) {
//         reject(new Error("Генерація числа не вдалася"));
//       } else {
//         resolve("Успішне виконання");
//       }
//     }, randomMs);
//   });
// }

// delay()
//   .then((result) => {
//     console.log(result);
//   })
//   .catch((error) => {
//     console.log(error);
//   });

// fetch("https://fakestoreapi.com/products")
//   .then((response) => {
//     return response.json();
//   })
//   .then((json) => {
//     console.log(json);
//   });

// fetch("https://fakestoreapi.com/products", {
//   method: "POST",
//   body: JSON.stringify({
//     title: "test product",
//     price: 13.5,
//     description: "lorem ipsum set",
//     image: "https://i.pravatar.cc",
//     category: "electronic",
//   }),
// })
//   .then((response) => {
//     return response.json();
//   })
//   .then((result) => {
//     console.log(result);
//   });

// fetch("https://fakestoreapi.com/products/7", {
//   method: "PATCH",
//   body: JSON.stringify({
//     title: "test",
//     price: 13.5,
//     description: "lorem ipsum set",
//     image: "https://i.pravatar.cc",
//     category: "electronic",
//   }),
// })
//   .then((response) => {
//     return response.json();
//   })
//   .then((result) => {
//     console.log(result);
//   });

// fetch("https://fakestoreapi.com/products/6", {
//   method: "DELETE",
// })
//   .then((response) => {
//     return response.json();
//   })
//   .then((result) => {
//     console.log(result);
//   })

// const API = "https://rickandmortyapi.com/api/";
// fetch(API)
//   .then((response) => {
//     return response.json();
//   })
//   .then((result) => {
//     fetch(result.characters)
//       .then((chResponse) => {
//         return chResponse.json();
//       })
//       .then((chResult) => {
//         console.log(chResult);
//       });
//   });

// async function getResult() {
//   const ep = await fetch("https://rickandmortyapi.com/api/episode");
//   const epJson = await ep.json();
//   console.log(epJson);
// }
// getResult();

// async function postData(url = "", data = {}) {
//   // Default options are marked with *
//   const response = await fetch(url, {
//     method: "POST", // *GET, POST, PUT, DELETE, etc.
//     mode: "cors", // no-cors, *cors, same-origin
//     cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
//     credentials: "same-origin", // include, *same-origin, omit
//     headers: {
//       "Content-Type": "application/json",
//       // 'Content-Type': 'application/x-www-form-urlencoded',
//     },
//     redirect: "follow", // manual, *follow, error
//     referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
//     body: JSON.stringify(data), // body data type must match "Content-Type" header
//   });
//   return response.json(); // parses JSON response into native JavaScript objects
// }

// postData("https://example.com/answer", { answer: 42 }).then((data) => {
//   console.log(data); // JSON data parsed by data.json() call
// });

// async function getItem() {
//   const todos = "https://jsonplaceholder.typicode.com/todos/";
//   const todo = await fetch(`${todos}4`);
//   const todoJson = await todo.json();
//   console.log(todoJson);
// }
// getItem();

// async function getPost() {
//   const posts = "https://jsonplaceholder.typicode.com/posts";
//   const post = await fetch(posts, {
//     method: "POST",
//     body: JSON.stringify({
//       title: "foo",
//       body: "bar",
//       userId: 1,
//     }),
//     headers: {
//       "Content-type": "application/json; charset=UTF-8",
//     },
//   });
//   const postJson = await post.json();
//   console.log(postJson);
// }
// getPost();

// async function getPut() {
//   const puts = "https://jsonplaceholder.typicode.com/posts/8";
//   const put = await fetch(puts, {
//     method: "PUT",
//     body: JSON.stringify({
//       id: 8,
//       title: "foo2",
//       body: "bar",
//       userId: 8,
//     }),
//     headers: {
//       "Content-type": "application/json; charset=UTF-8",
//     },
//   });
//   const putJson = await put.json();
//   console.log(putJson);
// }
// getPut();

// async function getPatch() {
//   const patches = "https://jsonplaceholder.typicode.com/posts/9";
//   const patch = await fetch(patches, {
//     method: "PATCH",
//     body: JSON.stringify({
//       title: "Vasya",
//     }),
//     headers: {
//       "Content-type": "application/json; charset=UTF-8",
//     },
//   });
//   const patchJson = await patch.json();
//   console.log(patchJson);
// }
// getPatch();

async function getDelete() {
  const API = "https://jsonplaceholder.typicode.com/posts/10";
  const del = await fetch(API, { method: "DELETE" });
  const delJson = await del.json();
  console.log(delJson);
}
getDelete();
