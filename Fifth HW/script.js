const users = "https://ajax.test-danit.com/api/json/users";
const posts = "https://ajax.test-danit.com/api/json/posts";
const list = document.createElement("div");
list.classList.add("cards");
document.body.append(list);

async function sendRequest(url, method, options) {
  const response = await fetch(url, { method: method, ...options });
  const result = await response.json();
  return result;
}

class card {
  constructor(element) {
    this.element = element;
  }

  async renderPost() {
    const postsCurency = await sendRequest(posts);
    return postsCurency;
  }
  async renderUser() {
    const usersCurrency = await sendRequest(users);
    return usersCurrency;
  }
  async render() {
    const posts = await this.renderPost();
    const users = await this.renderUser();
    users.forEach((user) => {
      let { id, name, email } = user;
      posts.forEach((post) => {
        let { body, userId, title, id: postID } = post;
        if (userId === id) {
          this.element.insertAdjacentHTML(
            "beforeend",
            `
         <div class ="card" data-id=${postID}>
         <div class = "card_header">
         <p>Заголовок: ${title}</p>
         <p>Текст: ${body}</p>
         </div>

         
         <div class = "card_footer">
         <p>Ім'я: ${name}</p>
         <p>Емейл: ${email}</p>
                  </div>
                  <button class ="delete_button">delete</button>
         </div>
          `
          );
        }
      });
    });
  }
}

const Card = new card(list);
Card.render();
