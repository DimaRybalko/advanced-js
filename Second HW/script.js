// 1 try...catch можна використовувати при роботі з сервером;
// коли в коді є незалежні один від одного функції і потрібно щоб все далі працювало а не зупинялось при першій помилці;
// При роботі з данними отриманими від користувача

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];
const correctArr = ["author", "name", "price"];

const div = document.getElementById("root");

const list = document.createElement("ul");

books.forEach((item, index) => {
  try {
    if (
      item.hasOwnProperty("author") &&
      item.hasOwnProperty("name") &&
      item.hasOwnProperty("price")
    ) {
      const listItems = document.createElement("li");
      listItems.innerHTML = `Назва:${item.name}
  Автор:${item.author}
  Ціна:${item.price}`;
      list.appendChild(listItems);
    } else {
      correctArr.forEach((key) => {
        if (!(key in item)) {
          throw new Error(`В об'єкті під номером ${index + 1} немає ${key}`);
        }
      });
    }
  } catch (error) {
    console.error(error.message);
  }
});

div.appendChild(list);
